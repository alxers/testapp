Testapp::Application.routes.draw do
  devise_for :users

  root :to => "home#index"

  resources :users

  post 'home/update' => 'home#update'
end
