# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create! :email => 'user@example.com', :password => 'secret12', :password_confirmation => 'secret12'
user2 = User.create! :email => 'user2@example.com', :password => 'secret12', :password_confirmation => 'secret12'
puts 'done'
