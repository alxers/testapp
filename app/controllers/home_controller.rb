class HomeController < ApplicationController
  before_filter :authenticate_user!

  def index
    @attachments = Attachment.all.reverse
  end

  def update
    current_user.attachments << Attachment.new(file: params[:file])
    current_user.save!
    redirect_to :root
  end
end
