class Attachment
  include Mongoid::Document
  include Mongoid::Timestamps
  
  attr_accessible :file

  mount_uploader :file, FileUploader

  #field :description, type: String
  #field :file, type: String
  
  belongs_to :user
end
